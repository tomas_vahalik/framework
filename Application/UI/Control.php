<?php
/**
 * Created by JetBrains PhpStorm.
 * User: straiki
 * Date: 7/11/12
 * Time: 12:07 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Straiki\Application\UI;


class Control extends \Nette\Application\UI\Control
{

    /**
     * Create template
     * @param string
     */
    public function createTemplate($class = NULL, $autosetFile = TRUE)
    {
        $template = parent::createTemplate($class);

        $this->context->template->configure($template);

        if ($autosetFile && !$template->getFile()) {
            $template->setFile($this->getTemplateFilePath());
        }

        return $template;
    }


    /**
     * Sets up template
     * @param $name
     */
    public function useTemplate($name)
    {
        $this->template->setFile($this->getTemplateFilePath($name));
    }


    /**
     * Derives template path from class name
     * @return string
     */
    protected function getTemplateFilePath($name = "")
    {
        $class = $this->getReflection();
        return dirname($class->getFileName()) . "/" . $class->getShortName() . ucfirst($name) . ".latte";
    }


    /********************* shortcuts *********************/


    /**
     * FlashMessage component
     * @return \Components\FlashMessageControl

    protected function createComponentFlashMessage()
    {
        return new \Components\FlashMessageControl;
    }
     * */


    /**
     * Context shortcut
     */
    final public function getContext()
    {
        return $this->parent->context;
    }


    /**
     * Model shortcut
     */
    final public function getModels()
    {
        return $this->context->models;
    }

}
