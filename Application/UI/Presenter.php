<?php
/**
 * Created by me for myself :)
 * Tomas Vahalik (c) <tomas@vahalik.cz>
 * Presenter.php
 * Date: 8/7/12
 * @author straiki
 * @package cms.webek
 */

namespace Straiki\Application\UI;

use Straiki;
use Nette\Security\User,
    Nette\Diagnostics\Debugger;
use Straiki\Traits\TWebLoader;

class Presenter extends \Nette\Application\UI\Presenter
{
    use TWebLoader;

    /** @var \Nette\Caching\Cache */
    public $cache;

    /** @var string */
    public $role = "";

    /** @var bool */
    public $logged = FALSE;

    /** @var string */
    private $referer;

    public function startup()
    {
        parent::startup();

        /** @var $helper my custom helpers */
        $helper = new \Straiki\Utils\Helpers($this->context);
        $this->template->registerHelperLoader(array($helper, 'loader'));

        // Registers panel with link to actual file (presenter, template...)
        //new \Straiki\Debug\PresenterLinkPanel($this->presenter);

        if (!$this->cache) {
            $this->cache = new \Nette\Caching\Cache($this->context->cacheStorage, 'presenter');
        }

        if ($this->user->isLoggedIn()) { //not logged in
            $this->logged = TRUE;
            $this->role = $this->user->getRole();
            $data = $this->user->getIdentity()->data;
            $this->template->username = $data['name'] . ' ' . $data['surname'];

            if (!$this->user->isAllowed($this->name, $this->action)) {
                if($this->user->isAllowed('Admin:Addpage', $this->action)){
                    $this->redirect(':Admin:Addpage:');
                }
//                if($this->user->isAllowed('Admin:UserPanel', $this->action)){
//                    $this->redirect(':Admin:UserPanel:');
//                }
                if($this->user->isAllowed('Front:Omluvenka', $this->action)){
                        $this->redirect(':Front:Omluvenka:');
                }
                $this->flashMessage('Access denied. You don\'t have permissions to view that page.', 'warning');
                $this->redirect(':Front:Sign:in');
            }else{
                if(isset($this->backlink))
                    $back = $this->getApplication()->restoreRequest($this->backlink);
                if(isset($back)){
                    $this->redirect($back);
                }

            }
        } else {
            if (!$this->user->isAllowed($this->name, $this->action)) {
                if ($this->user->getLogoutReason() == User::INACTIVITY) {
                    $this->flashMessage('Vaše přihlášení vypršelo!', 'warning');
                }
                $this->flashMessage('Access denied. You don\'t have permissions to view that page.', 'warning');
                $backlink = $this->getApplication()->storeRequest(); //@todo deprecated
                $this->redirect(':Front:Sign:in', array('backlink' => $backlink));
            }else{

            }
        }

    }

    /**
     * @param null $class
     * @return \Nette\Templating\ITemplate
     */
    public function createTemplate($class = NULL)
    {
        $template = parent::createTemplate($class);
        $this->context->template->configure($template, $this->lang);
        return $template;
    }

    /**
     * Models shortcut
     * @return \SystemContainer_models
     */
    public function getModels()
    {
        return $this->context->models;
    }


    /**
     * Translates flash messages
     * @param $message
     * @param string $type
     * @return stdClass
     */
    public function flashMessage($message, $type = "info") {
        if ($this->context->hasService("translator")) {
            $message = $this->getContext()->translator->translate($message);
        }

        return parent::flashMessage($message, $type);
    }


    /**
     * @return \Straiki\Security\User|object
     */
    public function getUser()
    {
        return $this->context->getByType('Straiki\Security\User');
    }

    /**
     * Update referer, if changed
     * @param \Nette\Session\SessionSection
     * @param \httpRequest
     */
    protected function updateReferer(&$session, $http)
    {
        $previous = $session->referer;

        // get this url
        $url = new Url($http->url);
        $url->query = NULL;
        $url = $url->absoluteUrl;

        // compare with this referer - drop shits
        $present = new Url($http->referer);
        $present->query = NULL;
        $present = $present->absoluteUrl;

        if ($present != $url OR empty($previous)) { // it's not the same, return new one
            $return = $present;
        }
        else {
            $return = $previous; // the same, return old one
        }

        $this->referer = $session->referer = $return;
    }

    /************ Additional Functions ************/


    //@TODO Vyhazet tu ten bordel

    /**
     * Method for uploading pictures to specific folder. If folder does not exists, it will create it.
     * If it's not writable, throws.
     * @param $picture
     * @param string $where 'upload/'
     * @param string $name
     * @param int $width
     * @param int $height
     * @param int $t_width
     * @param int $t_height
     * @param bool $FLAGS TRUE = Absolute path; FALSE = relative path
     * @return string array
     * @throws \Nette\DirectoryNotFoundException
     */
    protected function saveUploadedImage(\Nette\Http\FileUpload $picture, $where = '', $name = null, $width = 500, $height = 500, $t_width = 200, $t_height = 135, $FLAGS = true)
    {
        //@todo: extensions check
        $upPath = UPLOAD_DIR . '/' . $where;
        $tmpPath = TEMP_DIR . '/files/';

        $filename = \Nette\Utils\Strings::webalize($picture->getName());
        $extension = substr($picture->getName(), strrpos($picture->getName(), "."), strlen($picture->getName()));

        $tmpName = substr(sha1($filename), 5, 10);
        $picture->move($tmpPath . 'tmp_' . $tmpName . $extension); // nakopiruju si ho pro zpracovani

        $image = \Nette\Image::fromFile($tmpPath . 'tmp_' . $tmpName . $extension);

        if(strrpos($name, $extension)){
            $newName = ($name) ? $name : $filename; //sets name
        }else{
            $newName = ($name) ? $name . $extension : $filename . $extension; //sets name
        }

        $image->resize($width, $height);

        if (!is_dir($upPath)) {
            if (!mkdir($upPath))
                throw new \Nette\DirectoryNotFoundException;
        }
        if (!is_dir($upPath . 'originals/')) {
            if (!mkdir($upPath . 'originals/'))
                throw new \Nette\DirectoryNotFoundException;
        }
        if (!is_dir($upPath . 'thumb/')) {
            if (!mkdir($upPath . 'thumb/'))
                throw new \Nette\DirectoryNotFoundException;
        }


        $image->save($upPath . 'originals/' . $newName); //original
        $image->resize($t_width, $t_height);
        $image->save($upPath . 'thumb/t_' . $newName);
        ///> basePath ends with slash

        // target => http://web.tld/path/
        if ($FLAGS)
            $targetPath = $this->context->httpRequest->url->basePath . 'userfiles/' . $where;
        else
            $targetPath = 'userfiles/' . $where;

        $img = $targetPath . 'originals/' . $newName;
        $thumb = $targetPath . 'thumb/t_' . $newName;

        return array($img, $thumb);

    }


}
