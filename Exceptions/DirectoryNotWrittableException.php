<?php

namespace Straiki\Exceptions;

/*
 * Made by me for myself :)
 * 2012 (c) Tomas Vahalik
 */

/**
 * DirectoryNotWrittableException
 *
 * @author Straiki
 */
class DirectoryNotWrittableException extends \Exception {
    
}
