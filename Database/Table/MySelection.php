<?php

namespace Straiki\Database\Table;
/**
 * Created by me for myself :)
 * Tomas Vahalik (c) <tomas@vahalik.cz>
 * MySelection.php
 * Date: 8/17/12
 * @author straiki
 * @package cms.webek
 */
class Selection extends \Nette\Database\Table\Selection
{

    public function resetParams()
    {
        $this->parameters = array();
    }

}
