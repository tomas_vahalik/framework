<?php

namespace Straiki\Database;
/**
 * Created by me for myself :)
 * Tomas Vahalik (c) <tomas@vahalik.cz>
 * Connection.php
 * Date: 8/17/12
 * @author straiki
 * @package cms.webek
 */

use Straiki;

class Connection extends \Nette\Database\Connection
{

    /**
     * Creates selector for table.
     * @param  string
     * @return Straiki\Database\Table\Selection
     */
    public function table($table)
    {
        return new Table\Selection($table, $this);
    }


}
