<?php

namespace Straiki\Templates;

use Straiki\Utils\Helpers,
    Straiki\Templates\MyMacros,
	Nette\Templating\Filters\Haml,
	Nette\Latte;

class TemplateService extends \Nette\Object
{

	/** @var \Nette\DI\Container */
	private $context;


	public function __construct(\Nette\DI\Container $context)
	{
		$this->context= $context;
	}


    /**
     * @param \Nette\Templating\FileTemplate $template
     * @param null $lang
     */
    public function configure(\Nette\Templating\FileTemplate $template, $lang = NULL)
	{
		$latte = new Latte\Engine;

		// translator
		if ($this->context->hasService("translator")) {
			if ($lang) {
				$this->context->translator->setLang($lang);
			}

			$template->setTranslator($this->context->translator);
		}

		// macros
        $compiler = $latte->getCompiler();
		MyMacros::install($compiler);

		// filters
		$template->registerFilter(new Haml);
		$template->registerFilter($latte);

		// helpers
		$helpers = new Helpers($this->context);
		$template->registerHelperLoader(array($helpers, "loader"));
	}

}
