<?php

/*
 *  Made by me for me ;)
 *  All rights reserved, Tomas Vahalik
 *  <tomas@vahalik.cz>
 */

namespace Straiki\Security;

use \Nette\Security\Permission,
    \Nette\Database\Table\Selection;

/**
 * Description of Authorizator
 * @package cms
 * @author straiki
 */
class Authorizator extends Permission {

    /**
     * @param \Nette\Database\Table\Selection $roles
     * @param \Nette\Database\Table\Selection $resources
     * @param \Nette\Database\Table\Selection $rules
     */
    public function __construct(Selection $roles, Selection $resources, Selection $rules) {
         // definition roles
        foreach ($roles as $role) {
            // pokud záznam nedědí od jiné role:
            if ($role->id_parent == NULL) {
                $this->addRole($role->name);
            }
            // pokud má role rodiče, přebírá po něm vlastnosti
            else {
                $this->addRole($role->name,$roles[$role->id_parent]['name']);
            }
        }

        // definition resources
        foreach ($resources as $resource) {
            $this->addResource($resource->name);
        }

        /*static resources*/
        $this->addResource('Front:Homepage');
        $this->addResource('Front:Sign','Front:Homepage');
        $this->addResource('Front:Register','Front:Homepage');
        $this->addResource('Front:Page','Front:Homepage');
        $this->addResource('Front:Users','Front:Homepage');
        $this->addResource('Front:Multimedia','Front:Homepage');
        $this->addResource('Front:Album','Front:Homepage');
        $this->addResource('Front:Search','Front:Homepage');
        $this->addResource('Error','Front:Homepage');
        $this->addResource('Front:Shop','Front:Homepage');
        //$this->addResource('Admin:Homepage');
        //$this->addResource('Admin:Acl','Admin:Homepage');
        $this->addResource('Admin:MenuControl','Admin:Homepage');
        //$this->addResource('Admin:Addpage','Admin:Homepage');
        $this->addResource('Admin:UsersControl','Admin:Homepage');
        $this->addResource('Admin:PhotoGallery','Admin:Homepage');
        $this->addResource('Admin:Slideshow','Admin:Homepage');
        $this->addResource('Admin:Newsletter','Admin:Homepage');
        $this->addResource('Admin:Shop','Admin:Homepage');
        $this->addResource('Admin:Editpage');
        $this->addResource('Front:ChangePass','Admin:Editpage');

        $this->addResource('Admin:Test','Admin:Homepage'); //@Todo: vyradit z produkcniho

        //grant access for guest for all Front pages
        $this->allow('guest','Front:Homepage',Permission::ALL);

        $this->allow('registred','Admin:Editpage',Permission::ALL);


        // definition rules
        // výchozí role administrátor může cokoliv
        $this->allow('admin',Permission::ALL,Permission::ALL);
        foreach ($rules as $rule) {
            $this->allow($rule->roles->name, $rule->resources->name, $rule->privileges->value);
        }
    }
}
