<?php
/**
 * Created by me for myself :)
 * Tomas Vahalik (c) <tomas@vahalik.cz>
 * User.php
 * Date: 8/7/12
 * @author straiki
 * @package cms.webek
 */
namespace Straiki\Security;

use Nette\Security\AuthenticationException;

class User extends \Nette\Security\User
{

    /**
     * @param \Nette\Security\IUserStorage $storage
     * @param \Nette\DI\Container $context
     */
    public function __construct(\Nette\Security\IUserStorage $storage, \Nette\DI\Container $context)
    {
        parent::__construct($storage, $context);
    }


    /**
     * Update user identity data
     * @param array
     */
    public function updateIdentity(array $values)
    {
        foreach ($this->identity->data as $key => $value) {
            if (array_key_exists($key, $values)) {
                $this->identity->{$key} = $values[$key];
            }
        }
    }

    /**
     * Get user role
     */
    public function getRole()
    {
        $roles = $this->roles;
        return array_pop($roles);
    }

    /**
     * @return timestamp
     */
    public function getExpiration()
    {
        return $this->getStorage()->getSession()->getSection('Nette.Http.UserStorage/')->expireTime;
    }

}