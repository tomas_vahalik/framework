<?php

namespace Straiki\Utils;

use Nette\Diagnostics\Debugger,
	Nette\Utils\Validators;

class Url extends \Nette\Object
{

    /** @var string */
    private static $bitLyClient_id = "681c44bf47ee631a6ca3221a116126fef2101e92";
    /** @var string */
    private static $bitLyClient_secret = "2356dd0631b1abe448de08080edc879a6a608dda";
    /** @var string */
    private static $bitLyLogin = "straiki";

	/** @var string */
	private static $bitLyKey = "R_8e2af4026d11bdf76ccdac82adbb9310";


	/**
	 * Converts url to bit.ly version
	 * @param string
	 * @throws \Exception
	 */
	public static function bitLy($url)
	{
		if (Validators::isUrl($url)) {

 			$url = urlencode($url); // makes & possible etc.
			$ping = "http://api.bitly.com/v3/shorten?login=".self::$bitLyLogin."&apiKey=".self::$bitLyKey."&longUrl=".$url;

			$file  = file_get_contents($ping);
			$data = json_decode($file);

			if ($data->data->url) {
				return $data->data->url;
			}

			return urldecode($url);
		}
		else { // wrong type
			throw \Exception("$url is not an url.");
		}
	}

}
