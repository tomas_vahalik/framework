<?php
/**
 * Created by me for myself :)
 * Tomas Vahalik (c) <tomas@vahalik.cz>
 * Cache.php
 * Date: 8/5/13
 * @author straiki
 * @package
 */

namespace Straiki\Caching;

use Nette;

/**
 * @property mixed cache
 */
class Cache extends Nette\Caching\Cache
{

}
