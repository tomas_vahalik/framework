<?php

/**
 * Form control for selecting date.
 *
 *  – compatible with jQuery UI DatePicker and HTML 5
 *  – works with DateTime
 *
 * @author   Jan Tvrdík
 * @version  2.2
 * @link     http://nette.merxes.cz/date-picker/
 */

namespace Straiki\Forms\Controls;

use Nette,
	Nette\Forms,
	Nette\Forms\IControl,
	DateTime;

class CloseButton extends Forms\Controls\Button implements Forms\ISubmitterControl
{

	/** @link    http://dev.w3.org/html5/spec/common-microsyntaxes.html#valid-date-string */
	const W3C_DATE_FORMAT = 'Y-m-d';

	/** @var     string link*/
	protected $link = "this";

	/** @var     string            class name */
	private $className = 'btn btn-danger confirmLink';



	/**
	 * Class constructor.
	 *
	 * @author   Tomas Vahalik
	 * @param    string            label
	 */
	public function __construct($caption = 'Close', $link)
	{
		parent::__construct($caption);
        $this->caption = $caption;
		$this->control->type = 'button';
        $this->link = $link;
	}

    /**
     * Generates control's HTML element.
     *
     * @author   Tomas Vahalik
     * @param null $caption
     * @return   \Nette\Utils\Html
     */
	public function getControl($caption = NULL)
	{
        $control = parent::getControl();

        $control = Nette\Utils\Html::el('a')->href($this->link)->setText($this->caption);

        $control->class[] = $this->className;
		return $control;
	}


    function
    isSubmittedBy()
    {
        // TODO: Implement isSubmittedBy() method.
    }

    function
    getValidationScope()
    {
        return null;
    }
}
