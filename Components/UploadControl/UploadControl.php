<?php

namespace Straiki\Components;

use Models\TableThread;
use Nette\Image;
use Nette\Utils\Finder;
use Straiki\Application\UI\Control;
use Nette;
use Straiki\Forms\Form,
    Nette\Diagnostics\Debugger;
use Straiki\Utils\Filer;



/**
 * Created by me for myself :)
 * Tomas Vahalik (c) <tomas@vahalik.cz>
 * UploadControl.php
 * @property mixed galleryDir
 * @property mixed presenter
 * @author straiki
 */
class UploadControl extends Control
{
    public $successMsg = 'Úsěšně uloženo!';
    public $failMsg = 'Nastala chyba!';

    public $id;

    /** @var \Nette\Database\Table\Selection */
    public $galleryModel;

    /** @var \Nette\Database\Table\Selection */
    public $galleryFileModel;


    /**
     * Make new form
     * @param TableThread $fileModel
     * @param TableThread $galleryModel
     */
    public function __construct(TableThread $fileModel, TableThread $galleryModel)
    {
        $this->galleryFileModel = $fileModel->getAll();
        $this->galleryModel = $galleryModel->getAll();

    }

    public function handleUpload()
    {
        // 0. capture fie upload result
        ob_start();
        $upload_handler = new UploadHandler();
        $jsonData = ob_get_clean();

        // magic here
        $file = json_decode($jsonData)->files[0];

        /** output data example:
        stdClass (6)
        name => "dnb_typography-1920x1080.jpg" (28)
        size => 366296
        url => "http://local.peloton.cz/files/dnb_typography-1920x1080.jpg" (58)
        thumbnail_url => "http://local.peloton.cz/files/thumbnail/dnb_typography-1920x1080.jpg" (68)
        delete_url => "http://local.peloton.cz/?file=dnb_typography-1920x1080.jpg" (58)
        delete_type => "DELETE" (6)
         */

        $image = Image::fromFile($file->url);

        // 1. save, resize and save
        $uniqueName = Filer::getUniqueName($this->galleryDir, $file->name);

        if (!is_dir($this->galleryDir)) {
            mkdir($this->galleryDir, 0777);
        }

        $params = array(
            'natural' => array(
                'width' => 1024,
                'height' => 768
            ),
            'thumbnail' => array(
                'width' => 160,
                'height' => 120
            )
        );

        foreach ($params as $type => $dimensions) {
            if ($type === "natural") {
                $image->resize($dimensions["width"], $dimensions["height"], Image::SHRINK_ONLY | Image::EXACT);
                $image->save($this->galleryDir . "/" . $uniqueName);

            } else {
                Filer::resizeToSubfolder($image, $this->galleryDir, $dimensions["width"], $dimensions["height"], $uniqueName);
            }
        }

        $this->id = $this->presenter->id;
        $data = $this->galleryModel->where('link', $this->id)->fetch();

        // 2. save to db
        $data = array(
            "photoalbums_id" => $data->id,
            "filename" => $uniqueName,
            "filename_orig" => $file->name,
            "path" => $this->getGalleryDir(false) . "/" . $uniqueName,
            "thumb" => $this->getGalleryDir(FALSE) . "w160_h120/" . $uniqueName
        );

        $this->galleryFileModel->insert($data);

        // 3. cleanup file
        unlink(WWW_DIR . "/files/" . $file->name);
        unlink(WWW_DIR . "/files/thumbnail/" . $file->name);
    }


    public function handleSort()
    {
        if(isset($_POST["data"])){
            $data = explode(",", $_POST["data"]);
            $i = 1;
            foreach ($data as $item) {
                $this->galleryFileModel->update(array("seq" => $i), $item);
                $i++;
            }
        }
    }
    public function handleChangeName()
    {
        if(isset($_POST["data"]) && isset($_POST["id"])){
            $data = strip_tags($_POST["data"]);
            $id = strip_tags($_POST["id"]);
            try{
                $this->galleryFileModel->where('id', $id)->update(array('name' => $data));
            }catch (\Exception $e)
            {
                Debugger::log("Fail " . $e->getMessage(), 'error');
            }
            $this->presenter->flashMessage("Záznam byl aktualizován!",'success');
            $this->presenter->invalidateControl('flashes');
        }
    }


    /**
     * @param int
     */
    public function handleDeleteFile($fileId)
    {
        $this->id = $this->presenter->id;
        if ($galleryFile = $this->galleryFileModel->select('filename')->where('id', $fileId)->fetch()) {
            foreach (Finder::findFiles($galleryFile->filename)->from($this->galleryDir) as $file) {
                if (is_file($file)) {
                    unlink($file);
                }
            }

            $this->galleryFileModel->where('id', $fileId)->delete($fileId);

//            $galleryItem = $this->galleryModel->item($this->id);

            $this->presenter->flashMessage("Záznam byl úspěšně smazán.","success");

        } else {
            $this->presenter->flashMessage("Tento záznam neexistuje.", "error");
        }

        if($this->presenter->isAjax())
        {
            $this->presenter->invalidateControl('photos');
            $this->presenter->invalidateControl('flashes');
//            $this->presenter->redirect("this", array("fileId" => NULL));

        }else{
            $this->presenter->redirect("this", array("fileId" => NULL));
        }
    }


    public function render()
    {
        $this->id = $this->presenter->id;
        $data = $this->galleryModel->where('link', $this->id)->fetch();

        $key = array(
            "photoalbums_id" => $data->id
        );
//        $this->template->galleryThumbDir = $this->getGalleryDir(FALSE) . "w160_h120/";
        $this->template->galleryFiles = $this->galleryFileModel->where($key)->order("id");
        $this->template->galleryDir = $this->galleryDir;
        $this->template->render();
    }


    /********************** helpers **********************/


    /**
     * @param bool
     */
    public function getGalleryDir($absolute = TRUE)
    {
        $this->id = $this->presenter->id;

        return ($absolute ? WWW_DIR : "") . "/soubory/fotogalerie/" . $this->id . "/";
    }


}
