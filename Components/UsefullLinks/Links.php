<?php

namespace Straiki\Components;

/**
 * Created by me for myself :)
 * Tomas Vahalik (c) <tomas@vahalik.cz>
 * SmallNews.php
 * Date: 8/30/12
 * @author straiki
 * @package cms.webek
 */
class Links extends \Straiki\Application\UI\Control
{
    /** @var \Models\News */
    private $newsModel;
    /** @var \Models\TableThread */
    private $newsThread;

    private $categories;
    private $links;

    public function setModel(\Models\News $m, \Models\TableThread $t)
    {
        $this->newsModel = $m;
        $this->newsThread = $t;
    }

    public function create($c, $l)
    {
        $this->categories = $c;
        $this->links = $l;
    }

    public function render()
    {
        $this->template->categories = $this->categories;
        $i = 1;
        $this->template->links = '';

        if ($this->links) {
            foreach ($this->links as $link) {
                $this->template->links{$i} = $link;
                $i++;
            }
        }

        $this->template->render();
    }
}
