<?php

/**
 * Navigation
 *
 * @author Jan Marek
 * @license MIT
 */

namespace Components;

use Nette\Application\UI\Control;

class Navigation extends Control
{

	/** @var NavigationNode */
	private $homepage;

	/** @var NavigationNode */
	private $current;

	/** @var bool */
	private $useHomepage = false;

	/** @var string */
	private $menuTemplate;

	/** @var string */
	private $breadcrumbsTemplate;

    /** @var bool */
    private $renderChildren = true;

	/**
	 * Set node as current
	 * @param NavigationNode $node
	 */
	public function setCurrentNode(NavigationNode $node)
	{
		if (isset($this->current)) {
			$this->current->isCurrent = false;
		}
		$node->isCurrent = true;
		$this->current = $node;
	}

	/**
	 * Add navigation node as a child
	 * @param string $label
	 * @param string $url
	 * @return NavigationNode
	 */
	public function add($label, $url)
	{
		return $this->getComponent('homepage')->add($label, $url);
	}

	/**
	 * Setup homepage
	 * @param string $label
	 * @param string $url
	 * @return NavigationNode
	 */
	public function setupHomepage($label, $url)
	{
		$homepage = $this->getComponent('homepage');
		$homepage->label = $label;
		$homepage->url = $url;
		$this->useHomepage = true;
		return $homepage;
	}

	/**
	 * Homepage factory
	 * @param string $name
	 */
	protected function createComponentHomepage($name)
	{
		new NavigationNode($this, $name);
	}

	/**
	 * Render menu
	 * @param bool $renderChildren
	 * @param NavigationNode $base
	 * @param bool $renderHomepage
	 */
	public function renderMenu($renderChildren = TRUE, $base = NULL, $renderHomepage = TRUE)
	{
		$template = $this->createTemplate()
			->setFile($this->menuTemplate ?: __DIR__ . '/menu.phtml');
		$template->homepage = $base ? $base : $this->getComponent('homepage');
		$template->useHomepage = $this->useHomepage && $renderHomepage;
		$template->renderChildren = $this->renderChildren;
		$template->children = $this->getComponent('homepage')->getComponents();
		$template->render();
	}

	/**
	 * Render full menu
	 */
	public function render()
	{
		$this->renderMenu();
	}

	/**
	 * Render main menu
	 */
	public function renderMainMenu()
	{
        $template = $this->createTemplate()
            ->setFile($this->menuTemplate ?: __DIR__ . '/mainMenu.latte');
        $renderChildren = FALSE;
        $template->useHomepage = $this->useHomepage && true;
        $template->homepage = $this->getComponent('homepage');
        $template->renderChildren = $this->renderChildren;
        $template->children = $this->getComponent('homepage')->getComponents();
        $template->render();
	}
    /**
     * Render sitemap
     */
    public function renderSiteMap()
    {
        $template = $this->createTemplate()
            ->setFile($this->menuTemplate ?: __DIR__ . '/siteMap.latte');
        $renderChildren = TRUE;
        $template->renderChildren = $this->renderChildren;
        $template->children = $this->getComponent('homepage')->getComponents();
        $template->render();
    }

	/**
	 * Render breadcrumbs
	 */
	public function renderBreadcrumbs()
	{
		if (empty($this->current)) {
			return;
		}

		$items = array();
		$node = $this->current;

		while ($node instanceof NavigationNode) {
			$parent = $node->getParent();
			if (!$this->useHomepage && !($parent instanceof NavigationNode)) {
				break;
			}

			array_unshift($items, $node);
			$node = $parent;
		}

		$template = $this->createTemplate()
			->setFile($this->breadcrumbsTemplate ?: __DIR__ . '/breadcrumbs.latte');

		$template->items = $items;
		$template->render();
	}

    /**
     * Render breadcrumbs
     */
    public function renderSubMenu()
    {
        $template = $this->createTemplate()
            ->setFile(__DIR__ . '/subMenu.latte');

        $node = $this->current;
        $i = 0;

        while ($node instanceof NavigationNode) {
            $parent = $node->getParent();
            if (!$this->useHomepage && !($parent instanceof NavigationNode)) {
                break;
            }
            $node = $parent;
            $i++;
        }
        $node = $this->current;

        switch($i){
            case 0:
                $template->parent = null;
                break;
            case 1:
                $template->parent = $node; // first level
                //dump($node->getParent());
                //die();
                break;
            case 2:
                $template->parent = $node->getParent();
                break;
            case 3:
                $template->parent = $node->getParent()->getParent();
                break;
            default:
                $template->parent = null;
                break;

        }
        /*
                $supParent = $node->getParent()->getParent();
                if($supParent->getParent() instanceof NavigationNode)
                    $template->parent = $node->getParent()->getParent();
                else
                    $template->parent = $node->getParent();
        */

        $template->render();
    }


    /**
	 * @param string $breadcrumbsTemplate
	 */
	public function setBreadcrumbsTemplate($breadcrumbsTemplate)
	{
		$this->breadcrumbsTemplate = $breadcrumbsTemplate;
	}

	/**
	 * @param string $menuTemplate
	 */
	public function setMenuTemplate($menuTemplate)
	{
		$this->menuTemplate = $menuTemplate;
	}

	/**
	 * @return NavigationNode
	 */
	public function getCurrentNode()
	{
		return $this->current;
	}

    /**
     * @param bool $bool
     */
    public function setRenderChildren($bool)
    {
        $this->renderChildren = $bool;
    }

}
