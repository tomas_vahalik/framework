<?php

/**
 * @author Schmutzka 
 */
namespace Components;

use Straiki\Utils\Neon;

class JsLoader extends \WebLoader\Nette\JavaScriptLoader
{
    public $path = "/js/temp";

	public function __construct($basePath, $configPart = "js")
	{
		$filesArray = Neon::loadConfigPart("header.neon", $configPart);

		// d($basePath);
		$files = new \WebLoader\FileCollection(WWW_DIR . "/js");
		$files->addFiles($filesArray);

		$compiler = \WebLoader\Compiler::createJsCompiler($files, WWW_DIR . $this->path);

		// $compiler->addFileFilter(new \Webloader\Filter\jsShrink); // - breaking some code

		parent::__construct($compiler, $basePath . $this->path);
	}

}