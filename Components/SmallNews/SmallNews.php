<?php

namespace Straiki\Components;

/**
 * Created by me for myself :)
 * Tomas Vahalik (c) <tomas@vahalik.cz>
 * SmallNews.php
 * Date: 8/30/12
 * @author straiki
 * @package cms.webek
 */
class SmallNews extends \Straiki\Application\UI\Control
{
    /** @var \Models\News */
    private $newsModel;
    /** @var \Models\TableThread */
    private $newsThread;

    public function setModel(\Models\News $m,\Models\TableThread $t)
    {
        $this->newsModel = $m;
        $this->newsThread = $t;
    }

    public function render()
    {
        $this->template->info = $this->newsModel->getLastInfo();
        $this->template->news = $this->newsThread->getAll()->order('date DESC')->where('info',0)->limit(4);
        $this->template->render();
    }
}
