<?php
/**
 * Created by me for myself :)
 * Tomas Vahalik (c) <tomas@vahalik.cz>
 * PagesEdit.php
 * Date: 8/10/12
 * @author straiki
 * @package cms.webek
 */

namespace Straiki\Components;

class NewsEdit extends \Straiki\Application\UI\Control
{
    /** @var \Nette\DI\Container */
    private $context;

    /** @var \Models\News */
    private $news;

    /** @var string */
    private $cssClass = null;

    /** @var \Nette\Caching\Cache */
    private $cache;

    public function setContext(\Nette\DI\Container $s)
    {
        $this->context = $s;
        $this->news = $this->context->models->news->getAll()->order('date DESC');

    }


    /**
     * @param string $cls
     */
    public function setCssClass($cls)
    {
        $this->cssClass = $cls;
    }


    public function render()
    {

        $newsData = $this->news;
        if($newsData){

            $this->template->cssClass = $this->cssClass;
            if (count($newsData) > 0) {
                $this->template->newsData = $newsData;
            } else {

            }
        }else{

        }
        //$this->template->setFile(__DIR__ . '/tablecontrols.latte');
        $this->template->render();
    }

    /************ HANDLERS *****************/

    public function handleDelete($id)
    {
        $id = (int)$id;
        //$data = $this->menuModel->getAll()->select('id,parent_id')->where('pages_id',$id); //get data
        try{
            $this->context->models->news->getById($id)->delete();
        }
        catch(\PDOException $e){
            \Nette\Diagnostics\Debugger::log($e->getMessage());
        }

        $this->flashMessage('Novinka úspěšně smazána.', 'notice');

        $this->redirect('this');
    }



}
