<?php

/**
 * @author Schmutzka 
 */
namespace Components;

use Straiki\Utils\Neon;

class CssLoader extends \WebLoader\Nette\CssLoader
{
    public $path = '/css/temp';

	public function __construct($basePath, $configPart = "css" )
	{
		$filesArray = Neon::loadConfigPart("header.neon", $configPart);

		$files = new \WebLoader\FileCollection(WWW_DIR . "/css");
		$files->addFiles($filesArray);

		$compiler = \WebLoader\Compiler::createCssCompiler($files, WWW_DIR . $this->path);

		$compiler->addFileFilter(new \Webloader\Filter\LessFilter);
        //$compiler->addFilter(new \Webloader\Filter\CssMinifyFilter);

		parent::__construct($compiler, $basePath . $this->path);
	}

}