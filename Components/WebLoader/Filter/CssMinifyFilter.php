<?php

namespace WebLoader\Filter;

/**
 * Created by me for myself :)
 * Tomas Vahalik (c) <tomas@vahalik.cz>
 * CssMinifyFilter.php
 * Date: 8/22/12
 * @author straiki
 * @package cms.webek
 */
class CssMinifyFilter
{
    /**
     * @var \Webloader\Filter\CssMin
     */
    private $cssmini;

    /**
     * @return \Webloader\Filter\CssMin
     */
    private function getCssMini()
    {
        if(empty($this->cssmini)){
            $this->cssmini = new \Webloader\Filter\CssMin();
        }
        return $this->cssmini;
    }

    /**
     * Invoke filter
     * @param string $code
     * @param \WebLoader\Compiler $loader
     * @param string $file
     * @return string
     */
    public function __invoke($code, \WebLoader\Compiler $loader)
    {
        return $this->getCssMini()->minify($code);
    }

}
