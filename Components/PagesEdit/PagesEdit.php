<?php
/**
 * Created by me for myself :)
 * Tomas Vahalik (c) <tomas@vahalik.cz>
 * PagesEdit.php
 * Date: 8/10/12
 * @author straiki
 * @package cms.webek
 */

namespace Straiki\Components;

use Models\Menu;
use Models\Pages;
use Nette\Caching\Cache;
use Nette\DI\Container;

/**
 * @property mixed models
 * @property mixed presenter
 */
class PagesEdit extends \Straiki\Application\UI\Control
{
    /** @var Pages */
    private $pages;

    /** @var Menu */
    private $menu;

    private $pagesModel;

    /** @var string */
    private $cssClass = null;

    protected $context;

    /** @var Cache */
    private $cache;

    /**
     * @param string $cls
     */
    public function setCssClass($cls)
    {
        $this->cssClass = $cls;
    }

    /**
     * @param Pages $s
     */
    public function setService(Menu $s, Pages $p)
    {
        $this->pagesModel = $p;
        $this->menu = $s->getAll()->order('seq');
        $this->pages = $p->getAll();
    }

    /**
     * @param Container $c
     */
    public function setContext(Container $c)
    {
        $this->context = $c;
    }


    public function setCache(Cache $c)
    {
        $this->cache = $c;
    }

    public function render()
    {

        $pageData = $this->menu;

        $this->template->cssClass = $this->cssClass;
        if ($pageData) {
            $this->template->menu = $pageData;
        } else {

        }
        //$this->template->setFile(__DIR__ . '/tablecontrols.latte');
        $this->template->render();
    }

    /************ HANDLERS *****************/

    public function handleDelete($id)
    {
        $id = (int)$id;
        //$data = $this->menuModel->getAll()->select('id,parent_id')->where('pages_id',$id); //get data
        $data = $this->pagesModel->query('SELECT `id`,`parent_id` FROM `menu` WHERE (`pages_id` = ' . $id . ') ORDER BY `seq`');
//        dump($data['id']);
        $this->menu->where('parent_id', $data['id'])->update(array('parent_id' => $data['parent_id'])); // update kids to get new parent

        $this->pages->where('id', $id)->delete(); // delete parent
        $this->models->menu->repairSeq(); // repair sequence
        $this->cache->clean(array(Cache::TAGS => array('menu/sort'))); // clean the cache
        $this->flashMessage('Stránka úspěšně smazána.', 'notice');

        if ($this->presenter->isAjax()) {
            $this->invalidateControl('ctrl');
            $this->invalidateControl('navigation');
        } else
            $this->redirect('this');
    }


    public function handleActivate($id)
    {
        $this->pages->where('id', $id)->update(array('visible' => 'yes'));
        $this->flashMessage('Stránka byla zpřístupněna.', 'notice');

        $this->cache->clean(array(Cache::TAGS => array('menu/sort')));
        $this->cache->clean(array(Cache::TAGS => array("pages/$id")));
//        $this->cache->clean(Cache::ALL);
        if ($this->presenter->isAjax()) {
            $this->invalidateControl('ctrl');
            $this->invalidateControl('menuEdit');
            $this->invalidateControl('navigation');
        } else
            $this->redirect('this');
    }

    public function handleDeactivate($id)
    {

        $this->pages->where('id', $id)->update(array('visible' => 'no'));
        $this->flashMessage('Stránka byla znepřístupněna.', 'notice');
        $this->cache->clean(array(Cache::TAGS => array('menu/sort')));
        if ($this->presenter->isAjax()) {
            $this->invalidateControl('ctrl');
            $this->invalidateControl('menuEdit');
            $this->invalidateControl('navigation');
        } else
            $this->redirect('this');
    }

}
