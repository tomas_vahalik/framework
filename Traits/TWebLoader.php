<?php

namespace Straiki\Traits;
/**
 * Created by me for myself :)
 * Tomas Vahalik (c) <tomas@vahalik.cz>
 * TWebLoader.php
 * Date: 1/31/14
 * @author straiki
 * @package
 */

trait TWebLoader {
    /**
     * @return \Components\CssLoader
     */
    protected function createComponentCss() {
        return new \Components\CssLoader($this->template->basePath);
    }

    /**
     * @return \Components\JsLoader
     */
    protected function createComponentJs() {
        return new \Components\JsLoader($this->template->basePath);
    }

    /**
     * @return \Components\CssLoader
     */
    protected function createComponentFrontCss() {
        return new \Components\CssLoader($this->template->basePath,'cssFront');
    }

    /**
     * @return \Components\JsLoader
     */
    protected function createComponentFrontJs() {
        return new \Components\JsLoader($this->template->basePath,'jsFront');
    }
    /**
     * @return \Components\CssLoader
     */
    protected function createComponentBackCss() {
        return new \Components\CssLoader($this->template->basePath,'cssBack');
    }
    /**
     * @return \Components\JsLoader
     */
    protected function createComponentBackJs() {
        return new \Components\JsLoader($this->template->basePath,'jsBack');
    }

}